#!/usr/bin/env python
# coding: utf-8

# # Alcohol Consumption Prediction by Students an SVM implementation
# #By- Aarush Kumar
# #Dated: November 10,2021

# In[1]:


from IPython.display import Image
Image(url='https://image.ibb.co/eqPopQ/alc.jpg')


# In[2]:


import pandas as pd 
import matplotlib.pyplot as plt
from sklearn import preprocessing
import numpy as np
import pylab as pl
import pandas as pd
import matplotlib.pyplot as plt 
get_ipython().run_line_magic('matplotlib', 'inline')
import seaborn as sns
from sklearn.utils import shuffle
from sklearn.svm import SVC
from sklearn.metrics import confusion_matrix,classification_report
from sklearn.model_selection import cross_val_score, GridSearchCV


# In[3]:


student_mat = pd.read_csv('/home/aarush100616/Downloads/Projects/Student Alcohol Consumption/Data/student-mat.csv')
student_mat


# In[4]:


display(student_mat[["school","sex","age","Dalc","Walc","health",]].groupby(["age"]).agg(["max",'mean',"min"]).style.background_gradient(cmap="Oranges_r"))


# In[5]:


student_mat['label']="1"
student_mat.head()


# In[6]:


student_por = pd.read_csv('/home/aarush100616/Downloads/Projects/Student Alcohol Consumption/Data/student-por.csv')
student_por 


# In[7]:


display(student_por[["school","sex","age","Dalc","Walc","health",]].groupby(["age"]).agg(["max",'mean',"min"]).style.background_gradient(cmap="Oranges"))


# In[8]:


student_por['label']="0"
student_por.head()


# In[9]:


#barplots showing the frequency of each category separated by label
plt.figure(figsize=[15,17])
fft=['school','sex','age','Mjob','Fjob','Medu','Fedu','Dalc','Walc','health','absences','G1','G2','G3','label']
n=1
for f in fft:
    plt.subplot(4,4,n)
    sns.countplot(x=f, hue='Pstatus', edgecolor="black", alpha=0.7, data=student_mat)
    sns.despine()
    plt.title("Countplot of {}  by Pstatus".format(f))
    n=n+1
plt.tight_layout()
plt.show()


# In[10]:


#barplots showing the frequency of each category separated by label
plt.figure(figsize=[15,17])
fft=['school','sex','age','Mjob','Fjob','Medu','Fedu','Dalc','Walc','health','absences','G1','G2','G3','label']
n=1
for f in fft:
    plt.subplot(4,4,n)
    sns.countplot(x=f, hue='Pstatus', edgecolor="black", alpha=0.7, data=student_por)
    sns.despine()
    plt.title("Countplot of {}  by Pstatus".format(f))
    n=n+1
plt.tight_layout()
plt.show()


# In[11]:


plt.figure(figsize=[15,4])
sns.countplot(x='Dalc', hue='age',edgecolor="black", alpha=0.7, data=student_mat)
sns.despine()
plt.title("Countplot of Dalc by age")
plt.show()


# In[12]:


fig = plt.figure(figsize=(16,10))
ax = fig.add_subplot(111)
dfg = student_mat.groupby('age').sum()['Dalc']
dfg.plot(kind='line', title='Dalc - workday alcohol consumption groupby age', fontsize=20)

plt.ylabel('Dalc ')
ax.title.set_fontsize(30)
ax.xaxis.label.set_fontsize(20)
ax.yaxis.label.set_fontsize(20)
print('Max: ' + str(dfg.max()) + ' ocurred in ' + str(dfg.loc[dfg == dfg.max()].index.values[0:]))
print('Max: ' + str(dfg.min()) + ' ocurred in ' + str(dfg.loc[dfg == dfg.min()].index.values[0:]))
print('Mean: ' + str(dfg.mean()))


# In[13]:


plt.figure(figsize=[15,4])
sns.countplot(x='Dalc', hue='age',edgecolor="black", alpha=0.7, data=student_por)
sns.despine()
plt.title("Countplot of Dalc by age")
plt.show()


# In[14]:


fig = plt.figure(figsize=(16,10))
ax = fig.add_subplot(111)
dfg = student_por.groupby('age').sum()['Dalc']
dfg.plot(kind='line', title='Dalc - workday alcohol consumption groupby age', fontsize=20)

plt.ylabel('Dalc ')
ax.title.set_fontsize(30)
ax.xaxis.label.set_fontsize(20)
ax.yaxis.label.set_fontsize(20)
print('Max: ' + str(dfg.max()) + ' ocurred in ' + str(dfg.loc[dfg == dfg.max()].index.values[0:]))
print('Max: ' + str(dfg.min()) + ' ocurred in ' + str(dfg.loc[dfg == dfg.min()].index.values[0:]))
print('Mean: ' + str(dfg.mean()))


# In[15]:


plt.figure(figsize=[15,4])
sns.countplot(x='Walc', hue='age',edgecolor="black", alpha=0.7, data=student_mat)
sns.despine()
plt.title("Countplot of Walc by age")
plt.show()


# In[16]:


fig = plt.figure(figsize=(16,10))
ax = fig.add_subplot(111)
dfg = student_mat.groupby('age').sum()['Walc']
dfg.plot(kind='line', title='Walc - weekend alcohol consumption groupby age', fontsize=20)

plt.ylabel('Walc ')
ax.title.set_fontsize(30)
ax.xaxis.label.set_fontsize(20)
ax.yaxis.label.set_fontsize(20)
print('Max: ' + str(dfg.max()) + ' ocurred in ' + str(dfg.loc[dfg == dfg.max()].index.values[0:]))
print('Max: ' + str(dfg.min()) + ' ocurred in ' + str(dfg.loc[dfg == dfg.min()].index.values[0:]))
print('Mean: ' + str(dfg.mean()))


# In[17]:


plt.figure(figsize=[15,4])
sns.countplot(x='Walc', hue='age',edgecolor="black", alpha=0.7, data=student_por)
sns.despine()
plt.title("Countplot of Walc by age")
plt.show()


# In[18]:


fig = plt.figure(figsize=(16,10))
ax = fig.add_subplot(111)
dfg = student_por.groupby('age').sum()['Walc']
dfg.plot(kind='line', title='Walc - weekend alcohol consumption groupby age', fontsize=20)

plt.ylabel('Walc ')
ax.title.set_fontsize(30)
ax.xaxis.label.set_fontsize(20)
ax.yaxis.label.set_fontsize(20)
print('Max: ' + str(dfg.max()) + ' ocurred in ' + str(dfg.loc[dfg == dfg.max()].index.values[0:]))
print('Max: ' + str(dfg.min()) + ' ocurred in ' + str(dfg.loc[dfg == dfg.min()].index.values[0:]))
print('Mean: ' + str(dfg.mean()))


# In[19]:


Data= student_mat.append([student_mat,student_por])
x = Data.iloc[:, [3]].values
Data


# ## Prediction

# In[20]:


##Convert string to numeric
from sklearn.preprocessing import LabelEncoder
le = LabelEncoder()
def FunLabelEncoder(df):
    for c in df.columns:
        if df.dtypes[c] == object:
            le.fit(df[c].astype(str))
            df[c] = le.transform(df[c].astype(str))
    return df


# In[21]:


Data = FunLabelEncoder(Data)
Data.info()
Data.iloc[0:4,:]


# In[22]:


from sklearn.model_selection import train_test_split
Y = Data['label']
X = Data.drop(columns=['label'])
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.15, random_state=9)


# In[23]:


print('X train shape: ', X_train.shape)
print('Y train shape: ', Y_train.shape)
print('X test shape: ', X_test.shape)
print('Y test shape: ', Y_test.shape)


# In[24]:


X_train


# In[25]:


Y_train


# In[26]:


X_test


# In[28]:


Y_test


# ## SVM (Support Vector Machine) classification
# 
# SVMs (Support Vector Machine) have shown a rapid proliferation during the last years. The learning problem setting for SVMs corresponds to a some unknown and nonlinear dependency (mapping, function) y=f(x)
# between some high-dimensional input vector x and scalar output y. It is noteworthy that there is no information on the joint probability functions, therefore, a free distribution learning must be carried out. The only information available is a training data set D=(xi,yi)∈X×Y,i=1, l, where l stands for the number of the training data pairs and is therefore equal to the size of the training data set D, additionally, yi is denoted as di, where d
# 
# stands for a desired (target) value. Hence, SVMs belong to the supervised learning techniques.
# 
# From the classification approach, the goal of SVM is to find a hyperplane in an N-dimensional space that clearly classifies the data points. Thus hyperplanes are decision boundaries that help classify the data points. Data points falling on either side of the hyperplane can be attributed to different classes.

# In[29]:


from sklearn.ensemble import BaggingClassifier
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import SVC

# We define the SVM model
svmcla = OneVsRestClassifier(BaggingClassifier(SVC(C=10,kernel='rbf',random_state=9, probability=True), 
                                               n_jobs=-1))

# We train model
svmcla.fit(X_train, Y_train)

# We predict target values
Y_predict2 = svmcla.predict(X_test)


# In[30]:


test_acc_svmcla = round(svmcla.fit(X_train,Y_train).score(X_test, Y_test)* 100, 2)
train_acc_svmcla = round(svmcla.fit(X_train, Y_train).score(X_train, Y_train)* 100, 2)


# In[31]:


# The confusion matrix
svmcla = confusion_matrix(Y_test, Y_predict2)
f, ax = plt.subplots(figsize=(5,5))
sns.heatmap(svmcla, annot=True, linewidth=0.7, linecolor='black', fmt='g', ax=ax, cmap="BuPu")
plt.title('SVM Classification Confusion Matrix')
plt.xlabel('Y predict')
plt.ylabel('Y test')
plt.show()


# In[32]:


model = pd.DataFrame({
    'Model': ['SVM'],
    'Train Score': [train_acc_svmcla],
    'Test Score': [test_acc_svmcla]
})
model.sort_values(by='Test Score', ascending=False)


# In[33]:


from sklearn.metrics import average_precision_score
average_precision = average_precision_score(Y_test, Y_predict2)

print('Average precision-recall score: {0:0.2f}'.format(
      average_precision))

